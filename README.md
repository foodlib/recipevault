# Global Recipe Vault

With the Svalbard Global Seed Vault saving seeds all over the world, who's going to save the lost dishes that disappear after families stop selling or cooking them? :cry:

Remember that famous Chinese delicacy that you loved 10 years ago and now it's gone? And you just wished you could take things into your own hands to recreate that same goodness?

And how do we make sure that all of these recipes don't get lost forever even if it's in a soft digital copy? 

## That's where we come in. :thumbsup:

We want everyone that has a secret family dish or maybe a dish that they've a different way of doing it to share with the rest of the world.

Well, it's better to have it out there than keep it as a secret!

## How do I start then?

​Don't worry. I was just as lost using GitLab or even GitHub when I started creating this. So you're not alone. :relieved: Here are the easy few steps you can follow:

1. Sign up for a [GitLab account](https://gitlab.com/users/sign_in)
2. Type your recipe in a Markdown format (.md) - I recommend [Typora](https://www.typora.io/)
3. Save it in a relevant dish folder in our main recipes folder
4. Voila! You're done! :smiley_cat:

Learn more from our [contribution guide](https://gitlab.com/foodlib/recipevault/blob/master/CONTRIBUTING.md) for a step by step guide on how to contribute in an orderly manner.

## So why use Git to do so?

​It's because it's such a brilliant way of storing and making sure the versions are accounted for! We don't want something to happen because of an accident. :scared: We want to be able to go back in time to get the last updates.

It's also a brilliant way of staying **open**. It's what makes this whole experience great. **Anyone** can come in to contribute and learn throughout the entire experience. And if I stop doing this, anyone would be able to continue or fork the project. :smile_cat:

## References & Tools

- [Typora](https://www.typora.io/) - Fantastic Markdown (.md) editor
- [GitKraken](https://www.gitkraken.com/) - View, edit and push updates to the Recipe Vault
- [GitLab.com](gitlab.com/) - For gracefully hosting the entire repo for us ​:smile_cat:​

