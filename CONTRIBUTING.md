# Let's add those recipes in the vault!

### It's real easy. :sunglasses:

- [ ] Download a Markdown editor - I like [Typora](https://www.typora.io/). It's easy to use!
- [ ] Join our group, [Food Library](https://gitlab.com/foodlib)
- [ ] Type out your recipe with [Typora](https://www.typora.io/) and save it as a Markdown (.md) with a unique, funky name or you can just write your own name! :smile:
- [ ] Find the Recipes folder
- [ ] Create a folder for your dish
      - However, if the dish has already been created, just add your Markdown (.md) into the folder


### And you're done. :smiley_cat:

You can now edit them whenever you found an improvement to your recipe and **thank you so much for contributing to the Recipe Vault! :v:**

Do share this with the rest of the world so that we can have an active repository of recipes from all over the world! ​:airplane:​​:light_rail:​